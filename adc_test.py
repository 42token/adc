import busio
import digitalio
import board
from adafruit_mcp3xxx.mcp3008 import MCP3008
from adafruit_mcp3xxx.analog_in import AnalogIn
from time import sleep

class lightSensor:
    # Inizializziamo la classe passandogli l'oggetto adc e fissiamo il canale fisico 
    # (eventualmente poi faremo anche quello intercambiabile)
    def __init__(self, mcp):
        self.chan = AnalogIn(mcp, MCP3008.pin_0)
        # Valori da manuale sensore a 3.3V (per averli pronti da usare)
        self.logRange = 5.0
        self.rawRange = 1024.0

    # Una volta inizializzato possiamo leggere i valori dal canale e fornire una funzione che trasformi il risultato
    def readLight(self):
        # Leggo il valore dall'ADC
        adcVal = self.chan.value

        # Trasformo il valore (formula da manuale)
        logLux = adcVal * self.logRange / self.rawRange
        # Conversione da scala logaritmica
        return 10**logLux 


if __name__ == "__main__":
    # QUESTO E' IL MAIN RIPRENDIAMO ESEMPIO GITHUB https://github.com/adafruit/Adafruit_CircuitPython_MCP3xxx/
    # create the spi bus
    spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)
    # create the cs (chip select)
    cs = digitalio.DigitalInOut(board.D5)
    # create the mcp object from MCP3008 class
    mcp = MCP3008(spi, cs)

    # create an analog input channel on pin 0
    #chan = AnalogIn(mcp, MCP3008.pin_0)

    # A questo punto abbiamo un oggetto mcp che definisce un ADC, lo useremo per i nostri sensori definiti sopra
    sensore = lightSensor(mcp) # Qui sto passando l'oggetto ADC

    # Leggiamo il valore dal sensore
    while(True):
        # Leggo
        light = sensore.readLight()
        # Aspetto 30s
        sleep(30)